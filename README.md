---
title: C++ from scratch
---

# C++ from scratch

a sample project to start C++ programming

## Why CMake?

See https://www.kitware.com/build-with-cmake-build-with-confidence/

* CMake lets developers spend more time writing code and less time figuring out the build system
* CMake is open source and free to use for any project

* CMake supports multiple development environments and compilers on the same project (e.g., Visual Studio IDE, QtCreator, JetBrains, vim, emacs, gcc, MSVC, clang, Intel)
* CMake supports multiple languages including C/C++/CUDA/Fortran/Python, and also supports running arbitrary custom commands as part of the build
* CMake supports continuous integration (CI) testing in concert with Jenkins, Travis, CircleCI, GitlabCI, and almost any CI system via CTest. Test results are displayed using CDash (www.cdash.org).
* CMake supports integration of 3rd party libraries into your project.

* CMake is the de facto standard for building C++ projects
* Many C++ projects are switching to CMake; it is the 6th fastest growing language on github according to the [2018 Octoverse report](https://octoverse.github.com/projects.html)
* CMake is a mature and well tested with a broad developer community, it has undergone continuous improvement since 2000

## How to install CMake

- Install `cmake` (e.g. with `brew install cmake`)
- Create a directory structure with `include` and `src`
- Put the cpp files in `src` and the h files in `include`
- Make a directory `build`

## Writing the configuration file

Create a file called CMakeLists.txt:

```
cmake_minimum_required(VERSION 3.0)
project(helloworld_project)

#Bring the headers, such as Greeter.h into the project
include_directories(include)
  
#Can manually add the sources using the set command as follows:
#set(SOURCES src/mainapp.cpp src/Greeter.cpp)
  
#However, the file(GLOB...) allows for wildcard additions:
file(GLOB SOURCES "src/*.cpp")
  
add_executable(helloworld ${SOURCES})
```

Your directory will look like this:

```
.
├── CMakeLists.txt
├── helloworld.cpp
├── include
│   └── Greeter.h
└── src
 ├── Greeter.cpp
 └── mainapp.cpp
```

## Generating the makefiles

Then:
```bash
cd build
cmake ..
```

`cmake` will generate the makefiles for you. Then, compile and link with

## Building

Then:
```bash
make
```

## Executing

Finally, execute the binary with

```bash
./helloworld
```
