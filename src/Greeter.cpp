#include <iostream>
#include "Greeter.h"
using namespace std;

Greeter::Greeter(string name):name(name){}

void Greeter::display(){
	cout << "Hello, '" << this->name << "'!" << endl;
}
